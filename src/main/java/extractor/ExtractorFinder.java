package extractor;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

public class ExtractorFinder

{

	public static Set<ExtractorDatosDNI> findDetectors(String path,String paquete) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		Set<ExtractorDatosDNI> detectors=new HashSet<>();
		for(File file: new File(path).listFiles())
		{
			if(!isClassFile(file)) continue;

			Class<?> c=Class.forName(paquete+"."+file.getName().replace(".class","")); //se debe pasar el path como si fuese un package

			if(isDetectorDNIClass(c))
			{
				ExtractorDatosDNI extractorDatosDNI= (ExtractorDatosDNI) c.getDeclaredConstructor().newInstance();
				detectors.add(extractorDatosDNI);
			}
		}
		return detectors;
	}

	public static ExtractorDatosDNI elegirImplmentacion(Set<ExtractorDatosDNI> implementaciones)
	{

		return implementaciones.iterator().next();
	}

	private static boolean isClassFile(File file) {
		return file.getName().endsWith(".class");
	}

	private static boolean isDetectorDNIClass(Class<?> c) {
		return ExtractorDatosDNI.class.isAssignableFrom(c);
	}

}
