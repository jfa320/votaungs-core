package coordinadorSolicitudes;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Set;
import entidades.DNI;
import extractor.ExtractorDatosDNI;
import extractor.ExtractorFinder;
import padron.PadronUniversitario;

public class CoordinadorSolicitudes 
{
	
	public boolean analizarVotante(String imagen,String tipoDNI) throws Exception {
		ExtractorDatosDNI extractorDatosDNI= ExtractorFinder.elegirImplmentacion(ExtractorFinder.findDetectors("\\src\\main\\java\\extractor","extractor"));
		System.out.println(extractorDatosDNI.extraerDatosDNI(imagen));


		PadronUniversitario padron= new PadronUniversitario() 
		{
			//Esto es la implementacion, no creamos clase porque no tenia sentido. 
			@Override
			public boolean existeEnPadron(String dni) 
			{				
				//Realizar consulta a ws Externo
				return true;
			}
		};
		return padron.existeEnPadron(extractorDatosDNI.extraerDatosDNI(imagen));	
	}
	
	public boolean estaEnPadron(String dni, Set<DNI> padron)
	{
		for(DNI d : padron)
			if(d.toString().equals(dni))
				return true;
		return false;
	}
	
	public static String extraerDatosDNI(String path)   {
		
		File file = new File(path); 
		String dni = "";
		String lineaActual;
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			while ((lineaActual = br.readLine()) != null) {
				dni= lineaActual;
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return dni;
		
	}
	
	
}
