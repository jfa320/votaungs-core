package userStories;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.HashSet;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;

import coordinadorSolicitudes.CoordinadorSolicitudes;
import entidades.DNI;

public class Story1
{
	private DNI dni1;
	private Set<DNI> padron;
	CoordinadorSolicitudes coordinador;
			
	@Before
	public void setUp() //Escenario
	{
		dni1 = new DNI (1);
		padron = new HashSet<>();
		padron.add(dni1);
		coordinador=new CoordinadorSolicitudes();
	}
	
	@Test
	public void testCA1()
	{
		assertTrue(coordinador.estaEnPadron("1", padron));
	}
	
	@Test
	public void testCA2()
	{
		assertFalse(coordinador.estaEnPadron("2", padron));
	}
	
	@Test
	public void testCA3()
	{
		assertFalse(coordinador.estaEnPadron("", padron));
	}
}
