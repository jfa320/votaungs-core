package userStories;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import extractor.ExtractorFinder;

public class Story2 {
	private final String pathSinImpl=new File("").getAbsolutePath().toString()+"\\src\\test\\java\\userStories";
	private final String pathUnaImpl=new File("").getAbsolutePath().toString()+"\\src\\test\\java\\testUnaImplementacion";
	private final String pathDosImpl=new File("").getAbsolutePath().toString()+"\\src\\test\\java\\testDosImplementacion";
	private final String pathErroneo=new File("").getAbsolutePath().toString()+"\\src\\test\\java\\testPathErroneo";

	private final String paqSinImpl="testSinImplementacion";
	private final String paqUnaImpl="testUnaImplementacion";
	private final String paqDosImpl="testDosImplementacion";
	private final String paqErroneo="testPathErroneo";

	@Test
	public void testSinImplementacion() throws ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
		assertEquals(ExtractorFinder.findDetectors(pathSinImpl,paqSinImpl).size(),0);
	}

	@Test
	public void testUnaImplementacion() throws ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
		assertEquals(ExtractorFinder.findDetectors(pathUnaImpl,paqUnaImpl).size(),1);
	}

	@Test
	public void testDosImplementacion() throws ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
		assertEquals(ExtractorFinder.findDetectors(pathDosImpl,paqDosImpl).size(),2);
	}

	@Test(expected = NullPointerException.class)
	public void testPathErroneo() throws ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
		assertEquals(ExtractorFinder.findDetectors(pathErroneo,paqErroneo).size(),0);

	}


}
